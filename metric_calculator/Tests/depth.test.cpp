#include "depth.h"
#include <gtest/gtest.h>

using namespace ::testing;

TEST(depth_tests,depth_test_succeeds)
{
    depth depth_metric(depth_metric_type::MAX);

    depth_metric.parse_line("#include function_lines.h");
    depth_metric.parse_line("#include <string>");
    depth_metric.parse_line("#include <vector>");
    depth_metric.parse_line("using namespace std;");
    depth_metric.parse_line("function_lines::function_lines() : line_count(1, 0)");
    depth_metric.parse_line("{");
    depth_metric.parse_line("number_of_lines=1;");
    depth_metric.parse_line("depth_count=0;");
    depth_metric.parse_line("number_of_functions=0;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("");
    depth_metric.parse_line("void function_lines::parse_line(const std::string &line )");
    depth_metric.parse_line("{");
    depth_metric.parse_line("    for(size_t i=0; i<line.length();i++)");
    depth_metric.parse_line("    {");
    depth_metric.parse_line("        if (line[i]=='{')");
    depth_metric.parse_line("            depth_count++;");
    depth_metric.parse_line("");
    depth_metric.parse_line("        if (line[i]=='}')");
    depth_metric.parse_line("        {");
    depth_metric.parse_line("            depth_count--;");
    depth_metric.parse_line("");
    depth_metric.parse_line("            if (depth_count==0)");
    depth_metric.parse_line("            {");
    depth_metric.parse_line("            line_count[number_of_functions]=number_of_lines;");
    depth_metric.parse_line("                number_of_functions++;");
    depth_metric.parse_line("                line_count.push_back (0);");
    depth_metric.parse_line("                number_of_lines=1;");
    depth_metric.parse_line("            }");
    depth_metric.parse_line("        }");
    depth_metric.parse_line("    }");
    depth_metric.parse_line("    if (depth_count>0)");
    depth_metric.parse_line("        number_of_lines++;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("");
    depth_metric.parse_line("float function_lines::average_lines_per_function()");
    depth_metric.parse_line("{");
    depth_metric.parse_line("    float average=0;");
    depth_metric.parse_line("    for (int i=0; i<number_of_functions;i++)");
    depth_metric.parse_line("    {");
    depth_metric.parse_line("        average+=line_count[i];");
    depth_metric.parse_line("    }");
    depth_metric.parse_line("");
    depth_metric.parse_line("    return average/number_of_functions;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("");


    ASSERT_EQ(1, depth_metric.metric_result());
}

using namespace ::testing;

TEST(depth_tests,depth_test_fails)
{
    depth depth_metric(depth_metric_type::MAX);
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}       average+=line_count[i];");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}    return average/number_of_functions;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
 ASSERT_EQ(0, depth_metric.metric_result());
}

TEST(depth_tests,average_depth_test_passes)
{
    depth depth_metric(depth_metric_type::AVERAGE);
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
 ASSERT_EQ(1, depth_metric.metric_result());
}

TEST(depth_tests,average_depth_test_fails)
{
    depth depth_metric(depth_metric_type::AVERAGE);
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}       average+=line_count[i];");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}    return average/number_of_functions;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}       average+=line_count[i];");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}    return average/number_of_functions;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("{");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}       average+=line_count[i];");
    depth_metric.parse_line("}    ");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}    return average/number_of_functions;");
    depth_metric.parse_line("}");
    depth_metric.parse_line("}");
 ASSERT_EQ(0, depth_metric.metric_result());
}
