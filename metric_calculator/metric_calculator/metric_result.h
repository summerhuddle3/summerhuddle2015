#ifndef METRIC_RESULT_H_INCLUDED
#define METRIC_RESULT_H_INCLUDED

#include "detailed_metric_result.h"

class metric_result
{
    virtual detailed_metric_result get_result() = 0;
};

#endif

    /* format:

    {"metric" : [
    {"name" : "cyclomatric",
     "value", 1,
     "range" : "< 50",
     "result" : "pass" | "fail"},
     ...
    ]}

    */
