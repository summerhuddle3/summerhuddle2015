#ifndef FUNCTION_LINES_INCLUDED
#define FUNCTION_LINES_INCLUDED

#include "metric_parser.h"
#include "metric_result.h"
#include <vector>


struct function_metric_type
{
    enum value {MAX , AVERAGE};
};


class function_lines : public metric_parser, public metric_result
{
public:
    function_lines(function_metric_type::value function_type); //This is the constructor

    virtual void parse_line(const std::string & line);
    float average_lines_per_function();
    int metric_result();
    virtual detailed_metric_result get_result();

private:
    int number_of_lines;
    int depth_count;
    int number_of_functions;
    std::vector <int> line_count;
    function_metric_type::value function_type;


};


#endif
