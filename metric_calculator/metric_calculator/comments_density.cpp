#include "comments_density.h"
#include <sstream>

const int comments_lower_bound = 5;
const int comments_upper_bound = 50;

using namespace std;

comments_density::comments_density() : cumulative_total(0), line_length(0), comment_chars(0)
{
}

void comments_density::parse_line(const std::string & line)
{
    line_length = line.length();

    cumulative_total += line_length;

    std::size_t position = line.find("//");

    if(position != std::string::npos)
    {
        comment_chars += line_length - position;
    }

    if(cumulative_total != 0)
    {
        percent_comments = static_cast<int>((comment_chars * 100) / cumulative_total);
    }
    else
    {
        percent_comments = -1;
    }

}

int comments_density::metric_result()
{
    if(percent_comments >= comments_lower_bound && percent_comments < comments_upper_bound)
    {
        return 1;
    }
    return 0;

}

detailed_metric_result comments_density::get_result()
{
    detailed_metric_result result;

    result.name = "Comment density";
    result.value = percent_comments;
    stringstream s;
    s << comments_lower_bound;
    string lower_bound_str = s.str();
    s.str(string());
    s.clear();
    s << comments_upper_bound;
    result.threshold = lower_bound_str + "% to " + s.str() + "%";
    result.result = metric_result();

    return result;
}
