<html>
<body>
<table border=1>

<?php

   $json_string = '{"metrics":[' . 
		  ' {' .
                  ' "Metric Name": "depth",' .
                  ' "Measured Value": 1,' .
                  ' "Threshold Value": "10-20",' .
                  ' "Pass/Fail": "fail"' .
	          ' },' . 
		  ' {' .
                  ' "Metric Name": "cyclo",' .
                  ' "Measured Value": 5,' .
                  ' "Threshold Value": "10-20",' .
                  ' "Pass/Fail": "pass"' .
	          ' }' .
                  ']}';

	$json_array = json_decode($json_string);
echo '<tr>';
foreach ($json_array->metrics[0] as $key => $value) {
	echo '<th>';
	echo  $key;
	echo '</th>';
}
echo '</tr>';

foreach ($json_array->metrics as $metric) {
	echo '<tr>';
	foreach ($metric as $key => $value) {
		echo '<td>';
		echo  $value;
		echo '</td>';
	}
	echo '</tr>';
}
?>
</table>
</body>
</html>